const util = require('util');
const { resolve, common } = require('./webpack.common');
const { entries } = require('./html.config');
const apiMocker = require('mocker-api');
const HtmlWebpackPlugin = require('html-webpack-plugin');
let devServer;
class MyPlugin {
  apply(compiler) {
    compiler.hooks.compilation.tap('MyPlugin', compilation => {
      HtmlWebpackPlugin.getHooks(compilation).afterEmit.tapAsync(
        'MyPlugin', // <-- Set a meaningful name here for stacktraces
        (data, cb) => {
          console.log(`html changed`);
          devServer.sockWrite(devServer.sockets, 'content-changed');
          cb(null, data);
        },
      );
    });
  }
}

common.mode('development');

common.devtool('eval-source-map');

common.plugin('html').use(HtmlWebpackPlugin, [
  {
    template: './src/index.html',
    title: '开发环境',
    output: 'index.html',
    chunks: ['global', 'app'],
  },
]);

common.plugin('html-hot').use(MyPlugin);

common.module.rule('stylus').use('style').loader('style-loader').before('css').end().uses.delete('postcss').end();

common.devServer
  .port(8080)
  .contentBase('./dist')
  .hot(true)
  .overlay(true)
  .clientLogLevel('error')
  .before((app, server) => {
    devServer = server;
    apiMocker(app, resolve('../mocker/index.js'), {
      proxy: {
        '/api/(.*)': 'https://postman-echo.com',
      },
      pathRewrite: {
        '^/api/': '/',
      },
      secure: true,
      changeHost: true,
    });
  });

const config = common.toConfig();

// console.log(util.inspect(config, { showHidden: false, depth: null }));

module.exports = config;
