# webpack-template

> 有一点应该明确，"编程风格"的选择不应该基于个人爱好、熟悉程度、打字工作量等因素，而要考虑如何尽量使代码清晰易读、减少出错。你选择的，不是你喜欢的风格，而是一种能够清晰表达你的意图的风格。这一点，对于 Javascript 这种语法自由度很高、设计不完全成熟的语言尤其重要。

prettiey 代码风格检查和代码格式化
eslint 代码质量检查(例如使用了某个变量却忘记了定义)和代码风格检查
在.vscode/setting.json 中"eslint.enable": false,来关闭 vscode 的 eslint 提示功能
prettiey 负责格式化，让Stylelint检查样式，让ESLint检查语法逻辑，完成了整个前端 Lint 工作流
# 统一换行符为 LF，有利于跨平台合作.

提交时转换为 LF，检出时不转换
git config --global core.autocrlf input
不允许提交包含 CRLF 换行符的文件
git config --global core.safecrlf true

# git 提交前自动格式化代码

使用 “husky+lint-staged+prettier”组合在 git 提交前格式化代码。

# JS/TS 规范

1. 左花括号（大括号）不要另起一行，除非是 json 文件。始终和函数名，变量名在同一行
2. 左右括号前后有个空格，特殊情况：函数声明和调用名时左括号前不能有空格
3. 赋值操作符和比较操作符前后要空格：如 var a = "123"
4. 语句结束要加分号
5. 不要使用 with 语句
6. 始终使用“===”而不要使用“==”来判断相等
7. 变量声明放在函数作用域或者全局作用域的顶部
8. 函数必须在调用前声明，尽管 JS 会进行变量提升
9. 避免使用全局变量，常量使用大写字母数字和下划线命名
10. 构造函数命名首字母大写
11. “++，--”等自增操作使用“+=，-=”代替
12. 代码块不要省略花括号。尤其是 if for 等语句在只有一行代码时可以省略花括号，也不要省略
